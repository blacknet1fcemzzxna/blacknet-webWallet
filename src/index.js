import React from 'react';
import ReactDOM from 'react-dom';
import { IntlProvider } from 'react-intl';
import { ConfigProvider } from 'antd';
import { Provider as MobxProvider } from 'mobx-react';
import Routes from './routes';
import { locale, localeMessage } from './utils/locale';
import stores from './stores';
import './less/index.less';

/* eslint-disable global-require */
if (!Intl.PluralRules) {
  require('@formatjs/intl-pluralrules/polyfill');
  require('@formatjs/intl-pluralrules/dist/locale-data/de');
}

if (!Intl.RelativeTimeFormat) {
  require('@formatjs/intl-relativetimeformat/polyfill');
  require('@formatjs/intl-relativetimeformat/dist/locale-data/de');
}

ReactDOM.render(
  <MobxProvider {...stores}>
    <IntlProvider locale={locale} messages={localeMessage[locale]}>
      <ConfigProvider>
        <Routes />
      </ConfigProvider>
    </IntlProvider>
  </MobxProvider>,
  document.getElementById('root'),
);
