import React from 'react';
import { Tabs } from 'antd';
import './index.less';
import { FormattedMessage } from '@/utils/locale';
import history from '../../utils/history';
import Overview from './TabPanes/Overview';
import Send from './TabPanes/Send';
import Lease from './TabPanes/Lease';
import CancelLease from './TabPanes/CancelLease';
import Transactions from './TabPanes/Transactions';
import Staking from './TabPanes/Staking';
import SignVerifyMessage from './TabPanes/SignVerifyMessage';
import Peers from './TabPanes/Peers';
import Blocks from './TabPanes/Blocks';
import MnemonicInfo from './TabPanes/MnemonicInfo';
import Config from './TabPanes/Config';
import AboutBlacknet from './TabPanes/AboutBlacknet';


const { TabPane } = Tabs;
const paneMap = {
  Overview: <Overview />,
  Send: <Send />,
  Lease: <Lease />,
  'Cancel Lease': <CancelLease />,
  Transactions: <Transactions />,
  Staking: <Staking />,
  'Sign/Verify Message': <SignVerifyMessage />,
  Peers: <Peers />,
  Blocks: <Blocks />,
  'Mnemonic info': <MnemonicInfo />,
  Config: <Config />,
  'About Blacknet': <AboutBlacknet />,
};
const tabNameMap = [
  'Overview', 'Send', 'Lease', { CancelLease: 'Cancel Lease' }, 'Transactions', 'Staking', { SignVerifyMessage: 'Sign/Verify Message' }, 'Peers',
  'Blocks', { MnemonicInfo: 'Mnemonic info' }, 'Config', { AboutBlacknet: 'About Blacknet' },
];

const findInTabNameMap = (data, type) => {
  const objArr = tabNameMap.filter((tab) => typeof tab === 'object');
  let res;
  if (type === 'key') {
    objArr.forEach((obj) => {
      const [key] = Object.keys(obj);
      if (obj[key] === data) {
        res = key;
      }
    });
  }
  if (type === 'value') {
    objArr.forEach((obj) => {
      if (obj[data] !== undefined) {
        res = obj[data];
      }
    });
  }
  return res;
};

export default class TabsPage extends React.Component {
  tabsList = tabNameMap.map((tab) => typeof tab === 'string' ? tab : tab[Object.keys(tab)[0]])

  componentDidMount() {
    if (!localStorage.account) {
      history.replace('/');
    }
  }

  onTabChange = (tab) => {
    if (tabNameMap.includes(tab)) {
      history.push(`/${tab}`);
    } else {
      history.push(`/${findInTabNameMap(tab, 'key')}`);
    }
  }

  render() {
    const { match: { params: { tabName } = {} } = {} } = this.props;
    let activityKey;
    if (tabName) {
      if (tabNameMap.some((tab) => tab === tabName)) {
        activityKey = tabName;
      } else {
        activityKey = findInTabNameMap(tabName, 'value');
      }
    }

    return (
      <div>
        <Tabs
          defaultActiveKey={activityKey}
          tabPosition="left"
          style={{ maxHeight: 686, color: 'var(--grey-text)' }}
          type="card"
          animated
          onTabClick={this.onTabChange}
        >
          {this.tabsList.map((i) => {
            const id = typeof i === 'string' ? i : i[Object.keys(i)[0]];
            return (
              <TabPane tab={<FormattedMessage id={id} />} key={id}>
                {paneMap[i]}
              </TabPane>
            );
          })}
        </Tabs>
      </div>
    );
  }
}
