import React from 'react';
import { FormattedMessage } from '@/utils/locale';
import { inject, observer } from 'mobx-react';

@inject('blacknetStore')
@observer
export default class AboutBlacknet extends React.Component {
  render() {
    const { nodeInfo } = this.props.blacknetStore;
    return (
      <div className="about">
        <div>
          Blacknet Version <span className='overview_version strong'>{nodeInfo.version}</span>.
        </div>
        <div>
          Copyright © 2018-2020 Blacknet Team.
        </div>
        <div>
          <span><FormattedMessage id="This software uses experimental cryptography." /></span>
        </div>
        <div>
          <span><FormattedMessage id="Visit" /></span>
          <a href="https://blacknet.ninja" target="_blank"> https://blacknet.ninja</a>
        </div>
        <div>
          <span> <FormattedMessage id="for further information about the software." /></span>
        </div>
        <div>
          <span><FormattedMessage id="For licensing information see" /></span>
          <a href="LICENSE.txt" target="_blank"> LICENSE</a> and
          <a href="3RD-PARTY-LICENSES.txt" target="_blank"> 3RD-PARTY-LICENSES</a>.
        </div>
      </div>
    )
  }

}