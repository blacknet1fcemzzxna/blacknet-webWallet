import React from 'react';
import axios from 'axios';
import {
  Table, Form, Input, Button,
} from 'antd';
import { FormattedMessage } from '@/utils/locale';
import { throttle, message } from '@/utils/helper';

const { Column } = Table;

function verifyNetworkAddress(address) {
  // ipv4 | ipv6 | tor | i2p
  if (Object.prototype.toString.call(address) === '[object String]'
      && address.length >= 7 && address.length <= 70) {
    return true;
  }
  return false;
}

function verifyNetworkPort(port) {
  if (/\d+/.test(port) && port >= 0 && port <= 65535) {
    return true;
  }
  return false;
}

class Peers extends React.Component {
  constructor(props) {
    super(props);
    this.state = { peerInfo: [] };
  }

  componentDidMount() {
    const timePeerInfo = throttle(this.getPeers, 1000);
    timePeerInfo();
  }

  getPeers = async () => {
    const peers = await axios.get('/peers');
    this.setState({ peerInfo: peers });
  }

  onDisconnect = async (peerid) => {
    const ret = await axios(`/disconnectpeer/${peerid}/true`);
    if (ret === true || ret === 'true') {
      await this.getPeers();
    }
  }

  checkAddress = (rule, value, callback) => {
    if (!verifyNetworkAddress(value)) {
      callback('Invalid address');
      return;
    }
    callback();
  }

  checkPort = async (rule, value, callback) => {
    if (!verifyNetworkPort(value)) {
      callback('Invalid port');
      return;
    }
    callback();
  }

  onSubmitAddPeer = (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        const { ip_address: address, ip_port: port } = values;
        let result = await axios(`/addpeer/${address}/${port}/true`);
        if (result === true || result === 'true') {
          result = 'Connected';
        }

        await this.getPeers();

        message.success(`${address} ${result}`, 'success');

        this.props.form.setFieldsValue({ ip_address: '' });
      }
    });
  };

  render() {
    const { peerInfo } = this.state;
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="peerinfo">
        <h3><FormattedMessage id="peers">Connected Peers</FormattedMessage></h3>
        <Table
          dataSource={peerInfo}
          rowKey="peerId"
          pagination={false}
        >
          <Column
            title={<FormattedMessage id="Index" />}
            align="left"
            render={(text, record, index) => index + 1}
            key="index"
          />
          <Column
            title={<FormattedMessage id="address">Address</FormattedMessage>}
            align="center"
            dataIndex="remoteAddress"
            key="remoteAddress"
          />
          <Column
            title={<FormattedMessage id="user agent" />}
            align="center"
            dataIndex="agent"
            key="agent"
          />
          <Column
            title={<FormattedMessage id="ping" />}
            align="center"
            dataIndex="ping"
            key="ping"
          />
          <Column
            title={<FormattedMessage id="time offset" />}
            align="center"
            dataIndex="timeOffset"
            key="timeOffset"
          />
          <Column
            title={<FormattedMessage id="ban score" />}
            align="center"
            dataIndex="banScore"
            key="banScore"
          />
          <Column
            title={<FormattedMessage id="direction" />}
            align="center"
            dataIndex="direction"
            render={(text) => text ? 'Outgoing' : 'Incoming'}
            key="direction"
          />
          <Column
            title={<FormattedMessage id="read">Read</FormattedMessage>}
            align="center"
            dataIndex="totalBytesRead"
            render={(text) => `${(text / 1048576).toFixed(2)} MiB`}
            key="totalBytesRead"
          />
          <Column
            title={<FormattedMessage id="written">Written</FormattedMessage>}
            align="center"
            dataIndex="totalBytesWritten"
            render={(text) => `${(text / 1048576).toFixed(2)} MiB`}
            key="totalBytesWritten"
          />
          <Column
            title={<FormattedMessage id="Disconnect" />}
            align="center"
            dataIndex="peerId"
            className="disconnect"
            render={(text) => <a href="#" onClick={() => this.onDisconnect(text)}>Disconnect</a>}
            key="peerId"
          />
        </Table>
        <div className="form add-peer">
          <legend><FormattedMessage id="add peer">Add Peer</FormattedMessage></legend>
          <Form layout="inline" onSubmit={this.onSubmitAddPeer}>
            <div className="field-row">
              <Form.Item
                label={<FormattedMessage id="address">Address</FormattedMessage>}
              >
                {getFieldDecorator('ip_address', {
                  rules: [{ validator: this.checkAddress }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: 'onSubmit',
                })(
                  <Input size="large" style={{ width: '496px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row">
              <Form.Item
                label={<FormattedMessage id="port">Port</FormattedMessage>}
              >
                {getFieldDecorator('ip_port', {
                  rules: [{ validator: this.checkPort }, { transform: (value) => value ? value.trim() : value }],
                  validateTrigger: 'onSubmit',
                  initialValue: '28453',
                })(
                  <Input size="large" style={{ width: '496px' }} autoComplete="off" />,
                )}
              </Form.Item>
            </div>
            <div className="field-row" style={{ display: 'flex', flexDirection: 'row-reverse' }}>
              <Form.Item>
                <Button type="primary" htmlType="submit" style={{ marginRight: '0' }}>
                  <FormattedMessage id="add peer">Add Peer</FormattedMessage>
                </Button>
              </Form.Item>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default Form.create()(Peers);
