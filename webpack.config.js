const path = require('path');
const HappyPack = require('happypack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const plugins = [new HappyPack({
  loaders: [
    {
      loader: 'babel-loader',
      exclude: /node_modules\/(?!react-intl|intl-messageformat|intl-messageformat-parser)/,
      options: {
        presets: [
          '@babel/preset-react',
        ],
        plugins: [
          ['@babel/plugin-proposal-decorators', { legacy: true }],
          ['@babel/plugin-proposal-class-properties', { loose: true }],
          '@babel/plugin-syntax-dynamic-import',
          // '@babel/plugin-proposal-optional-chaining',
          // '@babel/plugin-proposal-nullish-coalescing-operator'
          ['import', {
            libraryName: 'antd',
            libraryDirectory: 'es',
            style: true,
          }],
        ],
      },
    },
  ],
})];
if (process.env.NODE_ENV === 'production') {
  plugins.unshift(new CleanWebpackPlugin());
}

plugins.push(new HtmlWebpackPlugin({
  template: './src/index.html',
}));
plugins.push(new CopyWebpackPlugin([
  { from: 'favicon.ico' },
  { from: 'assets/images', to: 'images' },
  { from: 'lib/js', to: 'js' },
  '3RD-PARTY-LICENSES.txt',
  'LICENSE.txt',
]));

module.exports = {
  mode: 'development',
  entry: {
    index: path.resolve(__dirname, './src/index.js'),
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    sourceMapFilename: '[file].map',
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'happypack/loader',
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: {
                hack: `true; @import "${path.resolve(__dirname, './src/less/antd-theme-rest.less')}";`,
              },
            },
          },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: [
                './src/less/variable.less',
              ],
            },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'img/[name].[hash:7].[ext]',
        },
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: 'fonts/[name].[ext]',
        },
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.less', '.jsx'],
    modules: [path.resolve(__dirname, './src'), 'node_modules'],
    alias: {
      '@': path.resolve(__dirname, './src'),
      _components: path.resolve(__dirname, './src/components'),
    },
  },
  plugins,
  devServer: {
    port: 9000,
    historyApiFallback: true,
    proxy: {
      '/api': 'http://localhost:8283',
    },
  },
};
